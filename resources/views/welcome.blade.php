<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact Form</title>
</head>
<body>
<h1>Contact Form</h1>

<form action="{{ route('user.create') }}" method="POST">
    @csrf
    <label for="name">Name:</label><br>
    <input type="text" id="name" name="name" required><br><br>

    <label for="email">Email:</label><br>
    <input type="email" id="email" name="email" required><br><br>

    <label for="message">Message:</label><br>
    <textarea id="message" name="message" rows="4" required></textarea><br><br>

    <button type="submit">Submit</button>
</form>

<hr>

<h2>Sent Messages</h2>
<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Message</th>
        <th>Created At</th>
    </tr>
    </thead>
    <tbody>
    @foreach($messages as $message)
        <tr>
            <td>{{ $message['name'] }}</td>
            <td>{{ $message['email'] }}</td>
            <td>{{ $message['message'] }}</td>
            <td>{{ $message['created_at'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</body>
</html>
