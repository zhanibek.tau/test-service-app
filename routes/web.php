<?php

use App\Http\Controllers\Healthcheck;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/')->group(function () {
    Route::get('', [UserController::class, 'getAllMessages'])->name('user.msgs');
});

Route::prefix('test')->group(function () {
    /**
     * Healthchecks for users and kubernetes
     */
    Route::prefix('healthcheck')->group(function () {
        Route::get('healthcheck', [Healthcheck::class, 'healthcheck']);
        Route::get('liveness', [Healthcheck::class, 'liveness']);
        Route::get('readiness', [Healthcheck::class, 'readiness']);
    });

// v1
    Route::prefix('v1')->group(function () {
        Route::prefix('user')->group(function () {
            Route::post('create', [UserController::class, 'createUser'])->name('user.create');
        });
    });
});
