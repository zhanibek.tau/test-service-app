<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResources extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id'                => $this->id,
            'name'              => (string) $this->name,
            'email'             => (string) $this->email,
            'message'           => (string) $this->message,
        ];
    }
}
