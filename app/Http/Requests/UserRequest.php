<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @OA\Schema(
 *    type="object",
 *    title="UserRequest",
 *    description="User request params",
 *
 *    @OA\Property(
 *      property="name",
 *      title="name",
 *      type="string"
 *    ),
 *    @OA\Property(
 *      property="email",
 *      title="email",
 *      type="string"
 *    ),
 *    @OA\Property(
 *      property="message",
 *      title="message",
 *      type="string"
 *    ),
 *    example={"message": "Hello world", "name": "Abai", "email": "Abai@mail.kz"}
 * )
 */
class UserRequest  extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required',
                'string',
            ],

            'email' => [
                'required',
                'string',
            ],

            'message' => [
                'required',
                'string',
            ],
        ];
    }
}
