<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Http\Resources\UserResources;
use App\Services\UserService;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    private UserService $userService;

    public function __construct(
        UserService $userService
    )
    {
        $this->userService = $userService;
    }

    /**
     *  @OA\Post(
     *     path="/test/v1/user/create",
     *     tags={"test v1"},
     *     summary="Create user by name, email and message",
     *     @OA\RequestBody(
     *       @OA\JsonContent(ref="#/components/schemas/UserRequest")
     *     ),
     *     @OA\Response(
     *         response="200",
     *         description="Order payment successefully placed in the shop",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="status",
     *                     type="integer"
     *                 ),
     *                 @OA\Property(
     *                     property="details",
     *                     type="object",
     *                     @OA\Property(
     *                          property="response",
     *                          type="string"
     *                     ),
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Invalid payload or code error is happened",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response="422",
     *         description="Validation error",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Full authentication is required to access this resource",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response="403",
     *         description="Access forbidden",
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     * )
     * @param UserRequest $request
     * @return mixed
     */
    public function createUser(UserRequest $request)
    {
        $requestData = $request->validated();
        $this->userService->createUser($requestData['name'], $requestData['email'], $requestData['message']);

        return redirect()->back()->with('success', 'Message sent successfully!');
    }

    public function getAllMessages()
    {
        $messages = $this->userService->getAllMessages()->toArray();
        return view('welcome', [
            'messages' => $messages
        ]);
    }
}
