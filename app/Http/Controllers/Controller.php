<?php

namespace App\Http\Controllers;

use App\Traits\ResponseTrait;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="Test Service (API)",
 *      description="<b>Описание:</b> Сервис включает функционал по получению информации по юзерам",
 * )
 *
 * @OA\Tag(
 *     name="Test Service",
 *     description="API Endpoints of Test"
 * )
 *
 */
class Controller extends BaseController

{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;
    use ResponseTrait;
}
