<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\JysanLoansService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class Healthcheck extends Controller
{
    private const SERVICE_NAME = 'test-service-app';

    /**
     * @OA\Get(
     *   path="/airport/healthcheck/healthcheck",
     *   tags={"health-check"},
     *   summary="Service healthchecking",
     *   @OA\Response(
     *      response=200,
     *      description="OK",
     *      @OA\Property(
     *          property="status",
     *          type="string",
     *      ),
     *      @OA\Property(
     *          property="text",
     *          type="string",
     *      )
     *   )
     * )
     */
    public function healthcheck(Request $request): JsonResponse
    {
        $statusCode = 200;
        $out = [
            'status' => 'ok',
            'text' => 'All operational normally',
        ];

        return new JsonResponse($out, $statusCode);
    }

    /**
     * @OA\Get(
     *   path="/airport/healthcheck/liveness",
     *   tags={"health-check"},
     *   summary="Service availability checking",
     *   @OA\Response(
     *      response=200,
     *      description="OK",
     *      @OA\Property(
     *          property="status",
     *          type="boolean",
     *      ),
     *      @OA\Property(
     *          property="data",
     *          type="array",
     *      )
     *   )
     * )
     */
    public function liveness(): array
    {
        return [
            'status' => true,
            'data' => [
                'service_name' => self::SERVICE_NAME,
                'pod_name' => $this->getPodName(),
            ],
        ];
    }

    /**
     * @OA\Get(
     *   path="/airport/healthcheck/readiness",
     *   tags={"health-check"},
     *   summary="Service healthchecking",
     *   @OA\Response(
     *      response=200,
     *      description="OK",
     *      @OA\Property(
     *          property="status",
     *          type="boolean",
     *      ),
     *      @OA\Property(
     *          property="data",
     *          type="array",
     *      )
     *   )
     * )
     */
    public function readiness(): array
    {
        return [
            'status' => true,
            'data' => [
                'service_name' => self::SERVICE_NAME,
                'pod_name' => $this->getPodName(),
            ],
        ];
    }

    /**
     * @return string
     */
    private function getPodName(): string
    {
        return env('HOSTNAME') ?? 'can_not_get_podname';
    }
}
