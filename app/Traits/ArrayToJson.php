<?php

namespace App\Traits;

trait ArrayToJson
{
    public function __set($key, $value): void
    {
        if (is_array($value)) {
            $value = json_encode($value, JSON_UNESCAPED_UNICODE);
        }
        parent::__set($key, $value);
    }
}
