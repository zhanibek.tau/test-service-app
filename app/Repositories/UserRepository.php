<?php

namespace App\Repositories;

use App\Models\User;
use App\Repositories\Eloquent\BaseRepository;
use Illuminate\Support\Collection;

class UserRepository extends BaseRepository
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $message
     * @return mixed
     */
    public function createUser(string $name, string $email, string $message)
    {
        return $this->model->create([
            'name' => $name,
            'email' => $email,
            'message' => $message
        ]);
    }

    /**
     * @return mixed
     */
    public function getAllMessages(): mixed
    {
        return $this->model->all();
    }
}
