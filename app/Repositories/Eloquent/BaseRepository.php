<?php

namespace App\Repositories\Eloquent;

use Closure;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BaseRepository implements IBaseRepository
{
    protected Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    public function find($id): ?Model
    {
        return $this->model->find($id);
    }

    public function findAll(): Collection
    {
        return $this->model->all();
    }

    public function findOrNew($id, $columns = ['*']): Model
    {
        return $this->model->findOrNew($id, $columns);
    }

    public function firstOrCreate(array $attributes = [], array $values = []): Model
    {
        return $this->model->firstOrCreate($attributes, $values);
    }

    public function firstOrFail(array $columns = ['*']): Model|ModelNotFoundException
    {
        return $this->model->firstOrFail($columns);
    }

    public function firstOr(array $columns = ['*'], Closure $callback = null): Model|Closure
    {
        return $this->model->firstOr($columns, $callback);
    }

    public function get(array $condition = []): Collection
    {
        return $this->model->where($condition)->get();
    }

    public function exists(array $condition): bool
    {
        return $this->model->where($condition)->exists();
    }

    public function update(int $id, array $requestData)
    {
        return $this->model->whereId($id)->update($requestData);
    }
}
