<?php

namespace App\Services;

use App\Repositories\UserRepository;

class UserService
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $message
     * @return mixed
     */
    public function createUser(string $name, string $email, string $message)
    {
        return $this->userRepository->createUser($name, $email, $message);
    }

    /**
     * @return mixed
     */
    public function getAllMessages(): mixed
    {
        return $this->userRepository->getAllMessages();
    }
}
